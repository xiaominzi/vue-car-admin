// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import ElementUI from 'element-ui'
import './assets/css/main.less'
import './assets/css/tables.less'
import './assets/css/variables.less'
import './assets/css/normalize.less'
import 'element-ui/lib/theme-default/index.css'
import './assets/css/layui.css'
import './assets/css/main.css'
import './assets/css/icon.css'

Vue.config.productionTip = false

Vue.use(ElementUI)

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})

import Vue from 'vue'
import Router from 'vue-router'
import lazyLoading from './lazyLoading'
import menuModule from '@/store/modules/menu'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Hello',
      name: 'Hello',
      component: lazyLoading('Hello'),
      meta: {
        label: 'welcome'
      }
    },
    {
      name: 'Home',
      path: '/',
      component: lazyLoading('Home'),
      meta: {
        label: '主页'
      },
      children: [
        ...generateRoutesFromMenu(menuModule.state.items)
      ]
    }
  ]
})
// Menu should have 2 levels.
function generateRoutesFromMenu (menu = [], routes = []) {
  for (let i = 0, l = menu.length; i < l; i++) {
    let item = menu[i]
    if (item.path) {
      routes.push(item)
    }
    if (!item.component) {
      generateRoutesFromMenu(item.children, routes)
    }
  }
  return routes
}

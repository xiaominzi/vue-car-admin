export default {
  user (val) {
    switch (val) {
      case 'id':
        return val
      case 'account':
        return '用户编号'
      case 'city':
        return '城市'
      case 'stage':
        return '阶段'
      case 'babyBirth':
        return '宝宝出生日期'
      case 'expectedDate':
        return '预产期'
      case 'lastMenstruationDate':
        return '最后月经日期'
      case 'menstruationPeriod':
        return '月经周期'
      case 'height':
        return '身高'
      case 'progestationWeight':
        return '孕前体重'
      case 'phone':
        return '手机号'
      case 'headImg':
        return '头像'
      case 'nickname':
        return '昵称'
      case 'sex':
        return '性别'
      case 'state':
        return '状态'
      case 'isShowActivityTips':
        return '是否显示活动提示'
      default:
        return val
    }
  },
  hospital (val) {
    switch (val) {
      case 'name':
        return '医院名称'
      case 'img':
        return '医院图片'
      case 'address':
        return '地址'
      case 'tagAdd':
        return '医院区域'
      case 'tagType':
        return '医院类型'
      case 'buildArchiveFee':
        return '建档费用'
      case 'antenatalCareFee':
        return '产前检查费用'
      case 'eutociaFee':
        return '顺产费用'
      case 'caesareanSectionFee':
        return '剖宫产费用'
      case 'attentionNum':
        return '关注人数'
      case 'lat':
        return '纬度'
      case 'lng':
        return '经度'
      case 'city':
        return '城市'
      case 'district':
        return '地区'
      case 'examineTs':
        return '唐筛'
      case 'examineWtfm':
        return '无痛分娩'
      case 'examineYscc':
        return '羊水穿刺'
      case 'examineDna':
        return 'DNA检查'
      case 'examineFmdl':
        return '分娩导乐'
      case 'remark':
        return '备注'
      case 'content':
        return '医院描述'
      case 'buildArchiveConditionAbbr':
        return '建档条件简介'
      case 'buildArchiveDescription':
        return '建档条件'
      case 'buildArchiveSchedule':
        return '建档排期'
      case 'buildArchiveProcess':
        return '建档流程'
      case 'abbrDescription':
        return '医院简介'
      case 'welfare':
        return '育果福利'
      case 'kind':
        return '当前分类'
      case 'state':
        return '是否显示'
      default:
        return val
    }
  },
  activity (val) {
    switch (val) {
      case 'name':
        return '活动名称'
      case 'startTime':
        return '开始时间'
      case 'city':
        return '城市'
      case 'address':
        return '地址'
      case 'joinNum':
        return '参加人数'
      case 'content':
        return '活动介绍'
      case 'abbrImg':
        return '图文简介图片'
      case 'img':
        return '活动详情图片'
      case 'sponsor':
        return '赞助商'
      case 'gift':
        return '听课礼'
      case 'isOffline':
        return '是否为线下活动'
      case 'type':
        return '活动介绍类型'
      case 'url':
        return '活动介绍外链'
      case 'joinUrl':
        return '报名加入外链'
      case 'createdTime':
        return '创建时间'
      default:
        return val
    }
  },
  preginfo (val) {
    switch (val) {
      case 'weekAt':
        return '孕期周数'
      case 'item':
        return '孕检项目'
      case 'description':
        return '孕检介绍'
      case 'content':
        return '富文本'
      case 'createdTime':
        return '创建时间'
      case 'checkTurn':
        return '第几次检查'
      default:
        return val
    }
  },
  gift (val) {
    switch (val) {
      case 'code':
        return '礼物码'
      case 'state':
        return '状态'
      case 'createdTime':
        return '创建时间'
      case 'getTime':
        return '领取时间'
      case 'userId':
        return '用户id'
      case 'name':
        return '礼券名称'
      case 'remark':
        return '备注'
      default:
        return val
    }
  }
}

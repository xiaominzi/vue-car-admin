// win.onload=function(){
function HTMLDeCode(str)
{
    var s = "";
    if(str.length == 0) return "";
    s = str.replace(/&/g,"&");
    s = s.replace(/</g,"<");
    s = s.replace(/>/g,">");
    s = s.replace(/ /g," ");
    s = s.replace(/'/g,"\'");
    s = s.replace(/"/g,"\"");
    return s;
}
function HTMLEnCode(str)
{
    var s = "";
    if(str.length == 0) return "";
    s = str.replace(/&/g,"&");
    s = s.replace(/</g,"<");
    s = s.replace(/>/g,">");
    s = s.replace(/ /g," ");
    s = s.replace(/\'/g,"'");
    s = s.replace(/\"/g,"\"");
    return s;
}

function alert_(content){
    layui.use('layer',function(){
        var layre=layui.layer;
        layer.alert(content)
    });
}
function msg_qunding(con,fun,fun2){
    var res;
    layui.use('layer', function(){
        layer.confirm(con, {
            btn: ['确认','取消'] //按钮
        }, function(){
            fun();
        }, function(){
            fun2();
        });
    });
    return res;
}

function space(str){
    var reg=/<p>(<br>|<p>|<\/p>)*<br><\/p>/gi;
    var str=str.replace(reg, '<p><br></p>');
    return str;
}
function htmlspecialchars(str){
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#039;');
    return str;
}
function htmlspecialchars1(str){
    str = str.replace(/&amp;/g, '&');
    str = str.replace(/&lt;/g, '<');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&quot;/g, '"');
    str = str.replace(/&#039;/g, '\'');
    return str;
}

//图片切换渐变
function jbimg(tab,img2){
    // $(tab).css('opacity','');
    // setInterval(colorLiner,20)
}
//活动探弹框获得卡片
function activity_get(length){
    var ind = length-1;
    // $('.activity_box').removeClass('block').eq(ind).fadeIn(1000);
    $('.activity_box').each(function(index) {
        if(index==ind){
            var shenka = $(this).find('.activity_box_tit').html();
            if($(this)[0].style.display=='block'){
                $(this).find('.activity_box_con2').html('获得两张“'+shenka+'”神器卡');
            }else{
                $(this).fadeIn(1000);
            }
        }
    });
}
function activity_main_close(){
    $('.activity2').css('display','none');
}
function activity_main_block(){
    $('.activity2').css('display','block');
}

//调用右提示函数
function zhiliao_layer(a,b){
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.tips(a, b);
    });
}
function zhiliao_layerzuo(a,b){
    layui.use('layer', function(){
        layer.tips(a, b, {
            tips: [4, '#000']
        });
    });
}
function layuimsg(a){
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.msg(errors[a]);
    });
};
//弹出登录提示
function loginAlert(this_){
    $('.tishidenglu').css('display','block');
    $('.jubao').off('click');
    $('.fankui_d').off('click');
    $('.ppc_a3').off('click');
    $('.search_btn').off('click');
    $(this_).off('click');
}
//认证弹框
function renzhengAlert(){
    $('.tishirenzheng_').css('display','block');
    $('.tishirenzheng_ .tishidenglu1 span').click(function(){
        $('.tishirenzheng_').css('display','none');
    });
    $('.jubao').off('click');
    $('.fankui_d').off('click');
    $('.ppc_a3').off('click');
}
//关闭弹框
function guanbi(this_,parent_){
    this_.click(function(){
        this_.parents(parent_).css('display','none');
        $('body').css('overflow','scroll');
    });
};
function block(obj,objParent){
    obj.on('click',function(){
        $('body').css('overflow','hidden');
        objParent.css('display','block');
    })
}
function setCookie(name,value){
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
function getCookie(name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}
function delCookie(name){
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null)
        document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}
//获取后台注册分享码
function shareGet(){
    var res;
    $.ajax({
        'url':'/user/sharereg',
        'type':'get',
        'data':{},
        'success':function(data){
            res = data;
        },
        'async':false
    });
    return res;
}
function msgcontent(c){
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.msg(c);
    });
}
function msgcontent1(c,length){
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.msg(c,{icon:length});
    });
}
function Trim(str)
{
    return str.replace(/(^\s*)|(\s*$)/g, "");
}
function fuzhi(){
    var Url2=document.getElementById("biao1");
    Url2.select(); // 选择对象
    document.execCommand("Copy"); // 执行浏览器复制命令
    msgcontent("已复制好，可贴粘！");
}
//发送标签排序 @cateids 所有选择标签的cateid每个id之间用 , 分割 例: 1,2,3,5
function cateSort(cateids){
    $.post('/user/cateSort',{'cates':cateids},function(result){
        if(result==0){
            msgcontent('系统繁忙请稍后再试');
        }else if(result==1){

        }
    });
}
function uc_biaoqian_follow(_biaoqian_id, _biaoqian_focus){
    $.post("/user/foLabel", {'cateid':_biaoqian_id, 'type':_biaoqian_focus}, function(result){
    });
}
function stopBubble1(e){
    debugger
    var e=e||window.event;
    if ( e.stopPropagation) {//非IE浏览器
        e.stopPropagation();// msgcontent('1')
    } else {//IE浏览器
        window.event.cancelBubble = true;//msgcontent('2')
    }
}

function getEvent(){
    if(window.event)    {return window.event;}
    func=getEvent.caller;
    while(func!=null){
        var arg0=func.arguments[0];
        if(arg0){
            if((arg0.constructor==Event || arg0.constructor ==MouseEvent
                || arg0.constructor==KeyboardEvent)
                ||(typeof(arg0)=="object" && arg0.preventDefault
                && arg0.stopPropagation)){
                return arg0;
            }
        }
        func=func.caller;
    }
    return null;
}
//阻止冒泡
function stopBubble()
{
    var e=getEvent();
    if(window.event){
        //e.returnValue=false;//阻止自身行为
        e.cancelBubble=true;//阻止冒泡
    }else if(e.preventDefault){
        //e.preventDefault();//阻止自身行为
        e.stopPropagation();//阻止冒泡
    }
}
function formsubmit(this_){
    $('fromTopic').submit();
}
/*var handlerPopup = function (captchaObj) {
 // 成功的回调
 captchaObj.onSuccess(function () {
 var validate = captchaObj.getValidate();
 $.ajax({
 url: "/user/ttt", // 进行二次验证
 type: "post",
 dataType: "json",
 data: {
 geetest_challenge: validate.geetest_challenge,
 geetest_validate: validate.geetest_validate,
 geetest_seccode: validate.geetest_seccode
 },
 success: function (data) {
 if (data && (data.status === "success")) {
 var username=$('.dl_main_yonghuming').val();
 var password=$('.dl_main_xinmima').val();
 var vcode=$('.dl_main_yanzheng').val();
 var rurl=$('.rurl').val();
 doLogin(username,password,vcode,rurl);
 } else {
 $(document.body).html('<h1>登录失败</h1>');
 }
 }
 });
 });
 $(".zhiliao_denglu").click(function () {
 var username=$('.dl_main_yonghuming').val();
 var password=$('.dl_main_xinmima').val();
 var vcode=$('.dl_main_yanzheng').val();
 var rurl=$('.rurl').val();
 //用户名验证
 if (!reg4.test(username)){
 zhiliao_layer(errors[0],'.dl_main_yonghuming');
 createCode();
 return false;
 };
 //密码验证
 if (!reg3.test(password)){
 zhiliao_layer(errors[4],'.dl_main_xinmima');
 createCode();
 return false;
 };
 //验证码验证
 if (vcode==''){
 zhiliao_layerzuo(errors[25],'.dl_main_yanzheng');
 createCode();
 return false;
 };
 captchaObj.show();
 });
 // 将验证码加到id为captcha的元素里
 captchaObj.appendTo("#popup-captcha");
 // 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
 };*/
var handlerPopupMobile = function (captchaObj) {
    $('#login_y').css('display','none');
    // 将验证码加到id为captcha的元素里
    captchaObj.appendTo("#popup-captcha-mobile");
    var reFre = function(){
        var validate = captchaObj.getValidate();
        $.ajax({
            url: "/user/ttt", // 进行二次验证
            type: "post",
            dataType: "json",
            data: {
                // 二次验证所需的三个值
                type: "mobile",
                geetest_challenge: validate.geetest_challenge,
                geetest_validate: validate.geetest_validate,
                geetest_seccode: validate.geetest_seccode
            },
            success: function (data) {
                if (data && (data.status === "success")) {
                    var username=$('.dl_main_zhanghao').val();
                    var password=$('.dl_main_xinmima').val();
                    var rurl=$('.rurl').val();
                    if(reg5.test(username)){
                        $('.loading').addClass('block');
                        $.post('/third/EngineerLogin',{'number':username,'password':password},function(resData){
                            $('.loading').addClass('none');
                            switch(resData){
                                case '5':
                                    msgcontent('请求方式错误');
                                    break;
                                case '1':
                                    window.location.href='/index';
                                    break;
                                case '0':
                                    msgcontent('系统出错，请稍后再试');
                                    break;
                                case '6':
                                    msgcontent('用户名或密码不正确');
                                    break;
                            }
                        });
                        return false;
                    }
                    //用户名验证
                    if (!reg4.test(username)){
                        captchaObj.onRefresh(reFre);
                        zhiliao_layer(errors[0],'.dl_main_zhanghao');
                        return false;
                    };
                    //密码验证
                    if (!reg3.test(password)){
                        captchaObj.onRefresh(reFre);
                        zhiliao_layer(errors[4],'.dl_main_xinmima');
                        return false;
                    };
                    var username=$('.dl_main_zhanghao').val();
                    var password=$('.dl_main_xinmima').val();
                    var rurl=$('.rurl').val();
                    var Vcode=$('.dl_main_yanzheng').val();
                    doLogin(username,password,rurl,Vcode);
                } else {
                    $(document.body).html('<h1>登录失败</h1>');
                }
            }
        });
    }
    function resetFFF(){
        captchaObj.refresh();
    }
    var CllThis = function () {
        var validate = captchaObj.getValidate();
        $.ajax({
            url: "/user/ttt", // 进行二次验证
            type: "post",
            dataType: "json",
            data: {
                // 二次验证所需的三个值
                type: "mobile",
                geetest_challenge: validate.geetest_challenge,
                geetest_validate: validate.geetest_validate,
                geetest_seccode: validate.geetest_seccode
            },
            success: function (data) {
                if (data && (data.status === "success")) {
                    /*var username=$('.dl_main_zhanghao').val();
                     var password=$('.dl_main_xinmima').val();
                     var rurl=$('.rurl').val();
                     //用户名验证
                     if (!reg2.test(username) && !reg.test(username)){
                     resetFFF();
                     zhiliao_layer(errors[0],'.dl_main_zhanghao');
                     return false;
                     };
                     //密码验证
                     if (!reg3.test(password)){
                     resetFFF();
                     zhiliao_layer(errors[4],'.dl_main_xinmima');
                     return false;
                     };
                     var username=$('.dl_main_zhanghao').val();
                     var password=$('.dl_main_xinmima').val();
                     var rurl=$('.rurl').val();
                     doLogin(username,password,rurl);*/
                } else {
                    $(document.body).html('<h1>登录失败</h1>');
                }
            }
        });
    };
    //拖动验证成功后两秒(可自行设置时间)自动发生跳转等行为
    captchaObj.onSuccess(CllThis);

    // 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
};
var handlerEmbed = function (captchaObj) {
    $("#embed-submit").click(function (e) {
        var validate = captchaObj.getValidate();
        if (!validate) {
            $("#notice")[0].className = "show";
            setTimeout(function () {
                $("#notice")[0].className = "hide";
            }, 2000);
            e.preventDefault();
            var username=$('.dl_main_yonghuming').val();
            var password=$('.dl_main_xinmima').val();
            var vcode=$('.dl_main_yanzheng').val();
            var rurl=$('.rurl').val();
            doLogin(username,password,vcode,rurl);
        }
    });
    // 将验证码加到id为captcha的元素里，同时会有三个input的值：geetest_challenge, geetest_validate, geetest_seccode
    captchaObj.appendTo("#embed-captcha");
    captchaObj.onReady(function () {
        $("#wait")[0].className = "hide";
    });
    // 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
};
/**
 * #验证密码需要根据某个条件来验证密码是否正确。
 * @type string 验证密码条件的类型，类型分为username(用户名)、phone(手机)、email(邮箱)
 * @value string|int 验证条件值，如果type是username此处应该为用户名而后台根据此用户名检测输入框内的密码是否与此用户密码匹配
 * @password string 固定值 'password'
 * @password string 输入框内的密码值 isPassword
 */
function isPassword(type,value,password,password1){
    var res;
    $.ajax({
        'url':'/user/isPassword',
        'data':{'type':type,'value':value,password:password1,'type2':password},
        'success':function(data){
            res = data;
        },
        'type': 'POST',
        'async':false
    });
    return res;
}

//注册  @phone手机可以为空 @email邮箱可以为空 @password 手机和邮箱必选其一
function sendEnroll(phone1,email1,password,share,type,pcode){
    $('.loading').addClass('block');
    var res;
    $.ajax({
        url:'/user/register',
        data:{'phone':phone1,'email':email1, 'password':password,'share':share,'type':type,'pcode':pcode},
        success:function(data){
            res = data;
            if(res == 7||res ==8){
                layuimsg(res);
                setTimeout(function(){
                    window.location.href = '/user/login';
                },2000);
            }else{
                layuimsg(res);
            }
        },
        'type': 'post',
        'async':true
    });
    $('.loading').removeClass('block');
}
//找回密码 @用户名 @email 此处表示手机或邮箱，不限于必须邮箱
/*function backPass(username,email){
 var res;
 $('.loading').addClass('block');
 $.ajax({
 url:'/user/redispass',
 data:{'username':username,'email':email},
 success:function(data){
 var res=data;
 var reg=/Could not authenticate/;
 if (res == 12){
 layuimsg(res);
 setTimeout(function(){
 window.location.href='/user/login';
 },2000);
 }else if (reg.test(res)){
 layuimsg('请求超时,服务器响应失败,请稍后重试');
 }else{
 layuimsg(res);
 }
 },
 'async':true,
 'type':'post'
 });
 $('.loading').removeClass('block');
 }*/
function backPass(email,phone,type,pcode){
    $('.loading').addClass('block');
    var res;
    $.ajax({
        url:'/user/redispass',
        data:{'phone':phone,'email':email,'type':type,'pcode':pcode},
        success:function(data){
            var res=data;
            var reg=/Could not authenticate/;
            if (res.success == 12){
                $('.loading').removeClass('block');
                layuimsg(res.success);
                setTimeout(function(){
                    window.location.href='/index/index';
                },2000);
            }else if (reg.test(res)){
                $('.loading').removeClass('block');
                layuimsg('请求超时, 服务器响应失败, 请稍后重试');
            }else if(res.success == 34){
                $('.loading').removeClass('block');
                setTimeout(function(){
                    window.location.href=res.url;
                },2000);
            }else if(res.success == 41){
                $('.loading').removeClass('block');
                msgcontent('验证码输入已达上限！');
            }else{
                $('.loading').removeClass('block');
                layuimsg(res.success);
            }
        },
        'async':true,
        'type':'post',
        'dataType':'json'
    });

}

//发送手机验证码
function PostPhoneCode(phone){
    var res;
    $.ajax({
        'url':'/user/PostPhoneCode',
        'data':{'phone':phone},
        'success':function(data){
            res = data;
        },
        'type': 'POST',
        'async':false
    });
    return res;
}

//判断验证码是否正确
function isPhoneCode(code){
    var res;
    $.ajax({
        'url':'/user/isPhoneCode',
        'data':{'regcode':code},
        'success':function(data){
            res = data;
        },
        'type': 'POST',
        'async':false
    });
    if (res==0){
        return false;
    }else if(res==1){
        return true;
    }
}
//登录
function doLogin(username,password,rurl,Vcode){
    $('.loading').addClass('block');
    var reg=/^[0-9]*$/;
    var res;
    $.ajax({
        url:'/user/dologin',
        data:{'username':username,'password':password,'rurl':rurl,'Vcode':Vcode},
        'async':true,
        'type':'post',
        success:function(data){
            res=data;
            if (!reg.test(res)){
                // if(window.localStorage){
                //     var str=window.localStorage;
                // }else{
                //     alert('亲，是时候升级你的老古董了');
                // }
                // if ($('#dl_main_jizhu').attr('checked')){
                //     // alert(1)
                //     str.setItem('user1',username);
                //     str.setItem('pass1',password);
                // }else{
                //     str.removeItem('user1');
                //     str.removeItem('pass1');
                // }
                if ($('#dl_main_jizhu').attr('checked')){
                    setCookie('user',username);
                    setCookie('pass',password);
                }else{
                    delCookie('user');
                    delCookie('pass');
                }

                window.location.href=res;
            }else if(res == '25'){
                layuimsg(res);
                $('.dl_main_img').click();
            }else{
                $('#login_C').html('');
                $('.dl_main_img').click();
                createCode();
                layuimsg(res);
            }
        }
    });
    $('.loading').removeClass('block');
    return res;
}

//重置验证
function reCode(){
    $.ajax({
        'url':'/user/reCode',
        'data':{},
        'success':function(data){
            res = data;
        },
        'type': 'POST',
        'async':false
    });
}
//验证码是否正确 @验证码框内value
function VcodeIs(codes){
    var res;
    $.ajax({
        'url':'/user/VcodeIs',
        'data':{'Code':codes},
        'success':function(data){
            res = data;
        },
        'type': 'GET',
        'async':false
    });
    if(res == 1){
        return true;
    }else{
        zhiliao_layerzuo(errors[25],'.dl_main_yanzheng');
        // $('.dl_main_img').click();
        // $('.dl_main_img').attr('src','/Vcode.php?'+Math.random();)
    }
}
//修改密码 @用户名 @当前密码 @密码
function editpass(password,pass){
    $('.loading').addClass('block');
    var res;
    $.ajax({
        'url':'/user/ModifyPass',
        'data':{'password':password,'pass':pass},
        'success':function(data){
            res = data;
            if(res == 16){
                layuimsg(res);
                setTimeout(function(){
                    window.location.href='/index/index';
                },2000);
                $('.loading').removeClass('block');
            }else if(res == 37){
                msgcontent('旧密码错误请确认！');
                $('.loading').removeClass('block');
            }else{
                zhiliao_layer(errors[res],'.dl_main_yanzheng');
                $('.loading').removeClass('block');
            }
        },
        'async':true,
        'type':'post'
    });
}

function isTimeOk(){
    $.post('/winning/isTimeOk',{},function(result){

    },'json');
}

function edwang(idid,imgsrc){
    var editor = new wangEditor(idid);
    // 上传图片
    editor.config.uploadImgUrl = imgsrc;
    editor.config.menus = [
        'bold',
        // 'underline',
        // 'italic',
        // 'strikethrough',
        'eraser',
        '|',
        'quote',
        'unorderlist',
        'orderlist',
        // 'alignleft',
        // 'aligncenter',
        // 'alignright',
        '|',
        'link',
        'unlink',
        'table',
        // 'emotion',
        '|',
        'img',
        // 'insertcode',
        '|',
        // 'undo',
        // 'redo',
        'fullscreen'
    ];
    editor.config.emotions = {
        // 支持多组表情

        // 第一组，id叫做 'default'


        // 第二组，id叫做'weibo'
        'weibo': {
            title: '微博表情',  // 组名称
            data: [{
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif",
                value : "[草泥马]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/60/horse2_thumb.gif",
                value : "[神马]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/fuyun_thumb.gif",
                value : "[浮云]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c9/geili_thumb.gif",
                value : "[给力]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f2/wg_thumb.gif",
                value : "[围观]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/70/vw_thumb.gif",
                value : "[威武]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6e/panda_thumb.gif",
                value : "[熊猫]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/81/rabbit_thumb.gif",
                value : "[兔子]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/otm_thumb.gif",
                value : "[奥特曼]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/15/j_thumb.gif",
                value : "[囧]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/89/hufen_thumb.gif",
                value : "[互粉]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c4/liwu_thumb.gif",
                value : "[礼物]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/ac/smilea_thumb.gif",
                value : "[呵呵]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/0b/tootha_thumb.gif",
                value : "[嘻嘻]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6a/laugh.gif",
                value : "[哈哈]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/14/tza_thumb.gif",
                value : "[可爱]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/af/kl_thumb.gif",
                value : "[可怜]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/a0/kbsa_thumb.gif",
                value : "[挖鼻屎]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f4/cj_thumb.gif",
                value : "[吃惊]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6e/shamea_thumb.gif",
                value : "[害羞]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c3/zy_thumb.gif",
                value : "[挤眼]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/29/bz_thumb.gif",
                value : "[闭嘴]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/71/bs2_thumb.gif",
                value : "[鄙视]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6d/lovea_thumb.gif",
                value : "[爱你]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/9d/sada_thumb.gif",
                value : "[泪]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/19/heia_thumb.gif",
                value : "[偷笑]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/8f/qq_thumb.gif",
                value : "[亲亲]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/b6/sb_thumb.gif",
                value : "[生病]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/58/mb_thumb.gif",
                value : "[太开心]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/17/ldln_thumb.gif",
                value : "[懒得理你]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/98/yhh_thumb.gif",
                value : "[右哼哼]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6d/zhh_thumb.gif",
                value : "[左哼哼]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/a6/x_thumb.gif",
                value : "[嘘]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/af/cry.gif",
                value : "[衰]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/73/wq_thumb.gif",
                value : "[委屈]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/9e/t_thumb.gif",
                value : "[吐]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f3/k_thumb.gif",
                value : "[打哈欠]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/27/bba_thumb.gif",
                value : "[抱抱]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7c/angrya_thumb.gif",
                value : "[怒]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/5c/yw_thumb.gif",
                value : "[疑问]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/a5/cza_thumb.gif",
                value : "[馋嘴]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/70/88_thumb.gif",
                value : "[拜拜]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/e9/sk_thumb.gif",
                value : "[思考]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/24/sweata_thumb.gif",
                value : "[汗]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7f/sleepya_thumb.gif",
                value : "[困]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6b/sleepa_thumb.gif",
                value : "[睡觉]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/90/money_thumb.gif",
                value : "[钱]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/0c/sw_thumb.gif",
                value : "[失望]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/40/cool_thumb.gif",
                value : "[酷]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/8c/hsa_thumb.gif",
                value : "[花心]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/49/hatea_thumb.gif",
                value : "[哼]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/36/gza_thumb.gif",
                value : "[鼓掌]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d9/dizzya_thumb.gif",
                value : "[晕]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/1a/bs_thumb.gif",
                value : "[悲伤]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/62/crazya_thumb.gif",
                value : "[抓狂]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/91/h_thumb.gif",
                value : "[黑线]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6d/yx_thumb.gif",
                value : "[阴险]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/89/nm_thumb.gif",
                value : "[怒骂]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/40/hearta_thumb.gif",
                value : "[心]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/ea/unheart.gif",
                value : "[伤心]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/58/pig.gif",
                value : "[猪头]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d6/ok_thumb.gif",
                value : "[ok]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d9/ye_thumb.gif",
                value : "[耶]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d8/good_thumb.gif",
                value : "[good]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c7/no_thumb.gif",
                value : "[不要]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d0/z2_thumb.gif",
                value : "[赞]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/40/come_thumb.gif",
                value : "[来]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d8/sad_thumb.gif",
                value : "[弱]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/91/lazu_thumb.gif",
                value : "[蜡烛]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6a/cake.gif",
                value : "[蛋糕]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/d3/clock_thumb.gif",
                value : "[钟]"
            }, {
                icon : "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/1b/m_thumb.gif",
                value : "[话筒]"
            }
            ]
        }
        // 下面还可以继续，第三组、第四组、、、
    };
    var content2 = $('#'+idid).html();
    var create_C = $("<input type='hidden' value='"+content2+"' name='"+idid+"'>");//reply_content
    $('#'+idid).parents('form').append(create_C);
    // onchange 事件
    editor.onchange = function () {
        var contet = this.$txt.html();
        $('input[name="'+idid+'"]').val(contet);
    };
    editor.config.pasteFilter = false;
    // editor.config.uploadImgFileName = 'myFileName';

    // var file = request.files['wangEditorH5File'];
    // var file = request.files['wangEditorFormFile'];
    // var file = request.files['wangEditorPasteFile'];
    // var file = request.files['wangEditorDragFile'];
    editor.create();
}

// }

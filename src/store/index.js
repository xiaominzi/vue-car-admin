import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import * as actions from './actions'
import user from './modules/user'
import resource from './modules/resource'
import menu from './modules/menu'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  getters,
  actions,
  modules: {
    user,
    resource,
    menu
  }
})

export default store

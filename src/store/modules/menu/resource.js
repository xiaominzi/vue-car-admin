import lazyLoading from './lazyLoading'

export default {
  name: 'Resource',
  // path: 'hospital',
  // component: lazyLoading('hospital', true),
  meta: {
    label: '资源管理',
    expanded: false
  },
  children: [
    // 医院列表
    {
      name: 'resourceList',
      path: 'resource',
      component: lazyLoading('resource/resourceList'),
      meta: {
        label: '资源列表'
      }
    },
    // 添加资源
    {
      name: 'resourceAdd',
      path: 'resource/add',
      component: lazyLoading('resource/resourceAdd'),
      meta: {
        label: '添加资源'
      }
    },
    // 医院详情
    {
      name: 'HospitalDetail',
      path: 'hospital/:id',
      component: lazyLoading('hospital/Detail'),
      meta: {
        label: '医院详情',
        hideInMenu: true
      }
    },
    // 编辑医院
    {
      name: 'HospitalEdit',
      path: 'hospital/:id/edit',
      component: lazyLoading('hospital/Edit'),
      meta: {
        label: '编辑医院',
        hideInMenu: true
      }
    },
    // 添加医院服务
    {
      name: 'HospitalSerivceAdd',
      path: 'hospital/:id/service/add',
      component: lazyLoading('hospital/service/ServicesForm'),
      meta: {
        label: '添加医院服务',
        hideInMenu: true
      }
    },
    // 医院服务详情
    {
      name: 'HospitalSerivceDetail',
      path: 'hospital/:id/service/:sid',
      component: lazyLoading('hospital/service/ServicesDetail'),
      meta: {
        label: '医院服务详情',
        hideInMenu: true
      }
    },
    // 编辑医院服务
    {
      name: 'HospitalSerivceEdit',
      path: 'hospital/:id/service/:sid/edit',
      component: lazyLoading('hospital/service/ServicesForm'),
      meta: {
        label: '编辑医院服务',
        hideInMenu: true
      }
    },
    // 活动管理
    {
      name: 'HospitalActivityAdd',
      path: 'hospital/:id/activity/add',
      component: lazyLoading('hospital/activity/AddActivities'),
      meta: {
        label: '添加医院服务',
        hideInMenu: true
      }
    }
  ]
}

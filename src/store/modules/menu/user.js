import lazyLoading from './lazyLoading'

export default {
  name: 'User',
  // path: 'user',
  // component: lazyLoading('user', true),
  meta: {
    label: '用户管理',
    expanded: false
  },
  children: [
    // 用户列表
    {
      name: 'UserList',
      path: 'user',
      component: lazyLoading('user/List'),
      meta: {
        label: '用户列表'
      }
    },
    // 用户详情
    {
      name: 'UserDetail',
      path: 'user/:id',
      component: lazyLoading('user/Detail'),
      meta: {
        label: '用户详情',
        hideInMenu: true
      }
    }
  ]
}

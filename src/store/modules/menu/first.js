import lazyLoading from './lazyLoading'

export default {
  name: 'First',
  // path: 'user',
  // component: lazyLoading('user', true),
  meta: {
    label: '后台首页',
    expanded: false
  },
  children: [
    // 用户列表
    {
      name: 'backstageIndex',
      path: 'backstageIndex',
      component: lazyLoading('index'),
      meta: {
        label: '后台首页'
      }
    }
  ]
}

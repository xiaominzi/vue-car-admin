import * as types from '../mutation-types'

const state = {
  // adminAccount: '',
  type: '',
  token: '',
  list: [],
  currentPage: 1,
  pageSize: 10,
  total: 0,
  sortField: '',
  sortMethod: 'DESC'
}

const mutations = {
  // [types.SET_ADMINACCOUNT] (state, data) {
  //   state.adminAccount = data.adminAccount
  // },
  // [types.CLEAR_ADMINACCOUNT] (state) {
  //   state.adminAccount = ''
  // },
  [types.SET_TOKEN] (state, data) {
    state.token = data.token
  },
  [types.CLEAR_TOKEN] (state) {
    state.token = ''
  },
  [types.SET_TYPE] (state, data) {
    state.type = data.type
  },
  [types.CLEAR_TYPE] (state) {
    state.type = ''
  },
  [types.SET_SYSTEM] (state, data) {
    state.list = data.list
    state.total = data.total
  },
  [types.SET_SYSTEM_CURRENTPAGE] (state, val) {
    state.currentPage = val
  },
  [types.SET_SYSTEM_PAGESIZE] (state, val) {
    state.pageSize = val
  },
  [types.SET_SYSTEM_SORT_FIELD] (state, val) {
    state.sortField = val
  },
  [types.TOGGLE_SYSTEM_SORT_METHOD] (state, val) {
    if (val) {
      console.log('val有值', val)
      state.sortMethod = val
    } else {
      console.log('val无值', val)
      state.sortMethod = state.sortMethod === 'DESC' ? 'ASC' : 'DESC'
    }
  }
}

const actions = {
  // export const setAdminAccount = ({commit}, val) => {
  //   commit(types.SET_ADMINACCOUNT, val)
  // }
  setToken ({commit}, val = '') {
    commit(types.SET_TOKEN, val)
  },
  setType ({commit}, val = '') {
    commit(types.SET_TYPE, val)
  },
  setSystem ({commit}, val) {
    commit(types.SET_SYSTEM, val)
  },
  setSystemCurrentPage ({commit}, val) {
    commit(types.SET_SYSTEM_CURRENTPAGE, val)
  },
  setSystemPageSize ({commit}, val) {
    commit(types.SET_SYSTEM_PAGESIZE, val)
  },
  setSystemSortField ({commit}, val) {
    commit(types.SET_SYSTEM_SORT_FIELD, val)
  },
  toggleSystemSortMethod ({commit}, val = '') {
    commit(types.TOGGLE_SYSTEM_SORT_METHOD, val)
  }
}

export default {
  state,
  mutations,
  actions
}

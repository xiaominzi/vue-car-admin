import * as types from '../mutation-types'

const state = {
  list: [],
  currentPage: 1,
  pageSize: 10,
  total: 0,
  sortField: '',
  sortMethod: 'DESC'
}
const mutations = {
  [types.SET_USER] (state, data) {
    state.list = data.list
    state.total = data.total
  },
  [types.SET_USER_CURRENTPAGE] (state, val) {
    state.currentPage = val
  },
  [types.SET_USER_PAGESIZE] (state, val) {
    state.pageSize = val
  },
  [types.SET_USER_SORT_FIELD] (state, val) {
    state.sortField = val
  },
  [types.TOGGLE_USER_SORT_METHOD] (state, val) {
    if (val) {
      console.log('val有值', val)
      state.sortMethod = val
    } else {
      console.log('val无值', val)
      state.sortMethod = state.sortMethod === 'DESC' ? 'ASC' : 'DESC'
    }
  }
}

const actions = {
  setUser ({commit}, data) {
    commit(types.SET_USER, data)
  },
  setUserCurrentPage ({commit}, val) {
    commit(types.SET_USER_CURRENTPAGE, val)
  },
  setUserPageSize ({commit}, val) {
    commit(types.SET_USER_PAGESIZE, val)
  },
  setUserSortField ({commit}, val) {
    commit(types.SET_USER_SORT_FIELD, val)
  },
  toggleUserSortMethod ({commit}, val = '') {
    commit(types.TOGGLE_USER_SORT_METHOD, val)
  }
}

export default {
  state,
  mutations,
  actions
}

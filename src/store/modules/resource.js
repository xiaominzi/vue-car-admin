import * as types from '../mutation-types'

const state = {
  list: [],
  currentPage: 1,
  pageSize: 10,
  total: 0,
  sortField: '',
  sortMethod: 'DESC'
}

const mutations = {
  [types.SET_HOSPITAL] (state, data) {
    state.list = data.list
    state.total = data.total
  },
  [types.SET_HOSPITAL_CURRENTPAGE] (state, val) {
    state.currentPage = val
  },
  [types.SET_HOSPITAL_PAGESIZE] (state, val) {
    state.pageSize = val
  },
  [types.SET_HOSPITAL_SORT_FIELD] (state, val) {
    state.sortField = val
  },
  [types.TOGGLE_HOSPITAL_SORT_METHOD] (state, val) {
    if (val) {
      console.log('val有值', val)
      state.sortMethod = val
    } else {
      console.log('val无值', val)
      state.sortMethod = state.sortMethod === 'DESC' ? 'ASC' : 'DESC'
    }
  }
}

const actions = {
  setHospital ({commit}, val) {
    commit(types.SET_HOSPITAL, val)
  },
  setHospitalCurrentPage ({commit}, val) {
    commit(types.SET_HOSPITAL_CURRENTPAGE, val)
  },
  setHospitalPageSize ({commit}, val) {
    commit(types.SET_HOSPITAL_PAGESIZE, val)
  },
  setHospitalSortField ({commit}, val) {
    commit(types.SET_HOSPITAL_SORT_FIELD, val)
  },
  toggleHospitalSortMethod ({commit}, val = '') {
    commit(types.TOGGLE_HOSPITAL_SORT_METHOD, val)
  }
}

export default {
  state,
  mutations,
  actions
}

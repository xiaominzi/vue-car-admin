const user = state => state.user
const resource = state => state.resource
const menuItems = state => state.menu.items
const token = state => state.app.token

export {
  user,
  resource,
  menuItems,
  token
}

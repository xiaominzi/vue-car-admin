import * as types from './mutation-types'
import api from '../api'
import axios from 'axios'

// 登录
export const login = ({commit}, params = {}) => {
  return api.login(params)
    .then(data => {
      console.debug('actions success', data)
      commit(types.SET_TOKEN, data)
      commit(types.SET_TYPE, data)
      var ltoken = data.token || ''
      window.sessionStorage.token = ltoken
      window.sessionStorage.type = data.type || ''
      axios.defaults.headers.common['Authorization'] = window.sessionStorage.token
    })
}

// 登出
export const logout = ({commit}, params) => {
  return api.logout(params = {})
    .then(data => {
      commit(types.CLEAR_TOKEN)
      commit(types.CLEAR_TYPE)
      window.sessionStorage.clear()
      return Promise.resolve()
    }, response => {
      return Promise.reject(response)
    })
}

export const expandMenu = ({ commit }, menuItem) => {
  if (menuItem) {
    menuItem.expanded = menuItem.expanded || false
    commit(types.EXPAND_MENU, menuItem)
  }
}

import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 活动列表
  getActivities (params = {}) {
    return axios.get('/activities', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 活动详情
  getActivitiesDetail (params = {}) {
    return axios.get('/activities/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 创建活动
  addActivities (params = {}) {
    console.log(params)
    return axios.post('/activities', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // edit 编辑活动
  editActivities (params = {}) {
    params.img = '/images/' + params.img.split('/images/')[1]
    params.abbrImg = '/images/' + params.abbrImg.split('/images/')[1]
    return axios.put('/activities/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除活动
  delActivities (params = {}) {
    return axios.delete('/activities/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}

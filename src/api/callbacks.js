export const successCallback = response => {
  console.debug('request success', response)
  let data = response.data
  return Promise.resolve(data.data)
}

export const failedCallback = error => {
  console.debug(error.response)
  let data = error.response.data
  if (data.errCode === 4010103) {
    alert(data.errMsg)
    window.sessionStorage.clear()
    window.location.href = '/yunyu/admin'
  } else {
    alert(data.errMsg)
  }
  return Promise.reject(data.errMsg)
}

export const errorCallback = error => {
  console.debug('request error', error)
  return Promise.reject(error)
}

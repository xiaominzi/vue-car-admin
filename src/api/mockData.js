export const hospitals = (params) => {
  let list = []
  let currentPage = params.page
  let pageSize = params.row

  for (let i = 1; i <= pageSize; i++) {
    let id = (currentPage - 1) * pageSize + i
    list.push({
      id,
      name: '用户' + id,
      phone: '13800138000',
      company: '昕海远洋',
      city: '北京',
      openId: 'em2dkc4e',
      team: '昕海远洋棒球队',
      isTeamLeader: false
    })
  }

  return {
    list,
    page: currentPage,
    row: pageSize,
    total: 100
  }
}

export const genPreginfoData = (params) => {
  let list = []
  let currentPage = params.page
  let pageSize = params.row

  for (let i = 1; i <= pageSize; i++) {
    let id = (currentPage - 1) * pageSize + i
    list.push({
      id,
      week: id,
      summary: '已经怀孕' + id + '周，你将进行第' + id + '次产检',
      items: 'B超、血型检查'
    })
  }

  return {
    list,
    page: currentPage,
    row: pageSize,
    total: 100
  }
}

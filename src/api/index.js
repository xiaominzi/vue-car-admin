/**
 * Mocking client-server processing
 */

import axios from 'axios'
import config from './config'
import * as cb from './callbacks'

import user from './user'
import hospital from './hospital'
import gift from './gift'
import preginfo from './preginfo'
import activity from './activity'
import system from './system'

// 本地模拟数据 用户列表
// import { genPreginfoData } from './mockData'

// 设置全局参数
axios.defaults.baseURL = config.url

export default {
  // 登录
  login (params = {}) {
    return axios.post('/token', params, {withCredentials: true}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 登出
  logout () {
    return axios.delete('/token').then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 功能管理
  getFunctions (params = {}) {
    return axios.get('/functions', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 功能详情
  getFunctionsDetail (params = {}) {
    return axios.get('/functions/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 权限管理
  getPermissions (params = {}) {
    return axios.get('/permissions', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 权限详情
  getPermissionsDetail (params = {}) {
    return axios.get('/permissions/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 添加权限
  addPermissions (params = {}) {
    return axios.post('/permissions', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 编辑权限
  editPermissions (params = {}) {
    return axios.put('/permissions/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 删除权限
  delPermissions (params = {}) {
    return axios.delete('/permissions/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 角色管理
  getRoles (params = {}) {
    return axios.get('/roles', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 角色详情
  getRolesDetail (params = {}) {
    return axios.get('/roles/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 添加角色
  addRoles (params = {}) {
    return axios.post('/roles', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 编辑角色
  editRoles (params = {}) {
    return axios.put('/roles/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 删除角色
  delRoles (params = {}) {
    return axios.delete('/roles/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 运营人员管理
  getAdmins (params = {}) {
    return axios.get('/admins', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 运营人员详情
  getAdminsDetail (params = {}) {
    return axios.get('/admins/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 添加运营人员
  addAdmins (params = {}) {
    return axios.post('/admins', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 编辑运营人员
  editAdmins (params = {}) {
    return axios.put('/admins/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 删除运营人员
  delAdmins (params = {}) {
    return axios.delete('/admins/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // 上传文件
  uploadFiles (params = {}) {
    return config.url + '/files'
  },
  // post 批量添加礼物码
  importCodes (params = {}) {
    // return axios.post('/codes/import', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
    return config.url + '/codes/import'
  },
  // 推送消息
  manualPush (params = {}) {
    return axios.post('/manualPush', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  ...user,
  ...hospital,
  ...gift,
  ...preginfo,
  ...activity,
  ...system
}

import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 用户列表
  getUsers (params = {}) {
    console.debug(params)
    return axios.get('/index.php?g=home&c=Repository&a=resourceList', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 用户详情
  getUsersDetail (params = {}) {
    return axios.get('/users/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除用户
  delUsers (params = {}) {
    return axios.delete('/users/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}

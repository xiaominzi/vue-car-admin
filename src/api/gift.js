import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 礼物列表
  getGifts (params = {}) {
    return axios.get('/codes', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 礼物详情
  getGiftsDetail (params = {}) {
    return axios.get('/codes/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 添加礼物
  addGifts (params = {}) {
    return axios.post('/codes', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // edit 编辑礼物
  editGifts (params = {}) {
    return axios.put('/codes/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除礼物
  delGifts (params = {}) {
    return axios.delete('/codes/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 礼券名称
  getGiftName (params = {}) {
    return axios.get('/gifts/name', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 礼券名称
  editGiftName (params = {}) {
    return axios.put('/gifts/name', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}

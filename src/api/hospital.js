import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 医院列表
  getHospitals (params = {}) {
    return axios.get('/hospitals', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 医院详情
  getHospitalsDetail (params = {}) {
    return axios.get('/hospitals/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 添加医院
  addHospitals (params = {}) {
    return axios.post('/hospitals', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 编辑医院
  editHospitals (params = {}) {
    params.img = '/images/' + params.img.split('/images/')[1]
    return axios.put('/hospitals/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除医院
  delHospitals (params = {}) {
    return axios.delete('/hospitals/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 医院服务列表
  getHospitalServices (params = {}) {
    return axios.get('/hospitalServices', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 医院服务详情
  getHospitalServicesDetail (params = {}) {
    return axios.get('/hospitalServices/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 添加医院服务
  addHospitalServices (params = {}) {
    return axios.post('/hospitalServices', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 编辑医院服务
  editHospitalServices (params = {}) {
    return axios.put('/hospitalServices/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除医院服务
  delHospitalServices (params = {}) {
    return axios.delete('/hospitalServices/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 医院活动列表
  getHospitalActivities (params = {}) {
    return axios.get('/hospitalActivities', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 绑定医院活动
  addHospitalActivities (params = {}) {
    return axios.post('/hospitalActivities', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除医院活动
  delHospitalActivities (params = {}) {
    return axios.delete('/hospitalActivities/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}

import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 权限-功能 关系列表
  getPermFuncRelations (params = {}, ...args) {
    return axios.get('/permissionFunctionRelations/' + args[0].id, {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 权限-功能 关系列表
  addPermFuncRelations (params = {}, ...args) {
    return axios.post('/permissionFunctionRelations/' + args[0].id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 权限-功能 关系列表
  delPermFuncRelations (...args) {
    return axios.delete('/permissionFunctionRelations/' + args[0].id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 角色-权限 关系列表
  getRolePermRelations (params = {}, ...args) {
    return axios.get('/rolePermissionRelations/' + args[0].id, {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 角色-权限 关系列表
  addRolePermRelations (params = {}, ...args) {
    return axios.post('/rolePermissionRelations/' + args[0].id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 角色-权限 关系列表
  delRolePermRelations (...args) {
    return axios.delete('/rolePermissionRelations/' + args[0].id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 管理员-角色 关系列表
  getAdminRoleRelations (params = {}, ...args) {
    return axios.get('/adminRoleRelations/' + args[0].id, {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 管理员-角色 关系列表
  addAdminRoleRelations (params = {}, ...args) {
    return axios.post('/adminRoleRelations/' + args[0].id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 管理员-角色 关系列表
  delAdminRoleRelations (...args) {
    return axios.delete('/adminRoleRelations/' + args[0].id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 角色-管理员 关系列表
  getRoleAdminRelations (params = {}, ...args) {
    return axios.get('/adminRoleRelations/' + args[0].id, {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 角色-管理员 关系列表
  addRoleAdminRelations (params = {}, ...args) {
    return axios.post('/adminRoleRelations/' + args[0].id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 角色-管理员 关系列表
  delRoleAdminRelations (...args) {
    return axios.delete('/adminRoleRelations/' + args[0].id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 修改密码
  changePass (params = {}) {
    return axios.put('/admins/password', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 重置密码
  resetPass (params = {}, ...args) {
    return axios.put('/admins/' + args[0].id + '/password', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}

var apiDev = 'http://139.129.209.170/carbook-tp/'
var apiProd = location.pathname.replace(/\/[a-z]*\/?$/, '') + '/api/admin'

console.debug('在api config js文件中', location)
console.debug('在api config js文件中', apiProd)

export default {
  url: process.env.NODE_ENV === 'production' ? apiProd : apiDev
}

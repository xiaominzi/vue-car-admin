import axios from 'axios'
import * as cb from './callbacks'

export default {
  // get 孕检信息列表
  getPreginfos (params = {}) {
    return axios.get('/pregnantInfos', {params}).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // get 孕检信息详情
  getPreginfosDetail (params = {}) {
    return axios.get('/pregnantInfos/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // post 添加孕检信息
  addPreginfos (params = {}) {
    return axios.post('/pregnantInfos/', params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // put 编辑孕检信息
  editPreginfos (params = {}) {
    return axios.put('/pregnantInfos/' + params.id, params).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  },
  // delete 删除孕检信息
  delPreginfos (params = {}) {
    return axios.delete('/pregnantInfos/' + params.id).then(cb.successCallback, cb.failedCallback).catch(cb.errorCallback)
  }
}
